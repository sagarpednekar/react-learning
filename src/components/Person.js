import React from 'react';
import './Person.css'

    // NOTE: Dosent Allow to change state of applications
const person = (props) =>{
   
    return(
        <div className="Person button">
        <p onClick={props.click}>I am {props.name}  & I am { props.age} years old</p>
        <p>{props.children}</p>
        <input type="text" onChange={ props.changeName} value= { props.name} />
        </div>
    )
};
export default person;