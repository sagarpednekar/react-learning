import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import Person from "./components/Person";
class App extends Component {
  constructor(props) {
    super(props);

  }
  state = {
    persons: [
      {
        name: "Sagar",
        age: 23
      },
      {
        name: "Shraddha",
        age: 24
      },
      {
        name: "Shweta",
        age: 25
      }
    ]
  };


  // define first handler
  handleClick = (name) => {
    this.setState({
      persons: [
        {
          name: name,
          age: 22
        },
        {
          name: "Shraddha Pednekar",
          age: 28
        },
        {
          name: "Shweta Pednekar",
          age: 25
        }
      ],
      showPersons: false,
      buttonTitle: 'show persons'
    })

  }
  personToggler = () => {
    const toggler = this.state.showPersons;
    this.setState({
      showPersons: !toggler
    })

  }
  nameChange = (event) => {
    this.setState({
      persons: [
        {
          name: event.target.value,
          age: 22
        },
        {
          name: "Shraddha Pednekar",
          age: 28
        },
        {
          name: "Shweta Pednekar",
          age: 25
        }
      ]
    })

  }

  render() {
    const person = this.state.persons;
    const inlineStyle = {
      background: 'orange',
      color: 'white'
    }
    let personsList = null;
    if (this.state.showPersons) {
      personsList = (
        <div>
          <Person name={person[0].name} age={person[0].age} changeName={this.nameChange} />
          <Person name={person[1].name} age={person[1].age} click={this.handleClick.bind(this, "Joyee")}>
            <pre>
              SHE LIKES MUSIC, DANCE & TREKKING
            </pre>
          </Person>
          <Person name={person[2].name} age={person[2].age} />
        </div>
      );
    }
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title"> Awsome code </h1>{" "}
        </header>
        { personsList }
        <button onClick={this.personToggler.bind(this)} style={inlineStyle}  >
          Change State
        </button>
      </div>
    );
  }
}

export default App;
